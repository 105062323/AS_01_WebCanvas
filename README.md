# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

# Report_105062323

## 結構
* 利用**addEeventListener**去偵測mouse的行為，在各個event裡呼叫處理控制的function，下面有例子
    ```javascript=
        canvas.addEventListener("mousemove", function (e) {
            EventContoller('move', e)
        }, false);
    ```

* 而**Event**分為以下四類，偵測的情形就與文字敘述相同，在偵測完滑鼠狀態後，會針對目前選定的工具用switch去呼叫不同function
    * mousedown
    * mouseup
    * mousemove
    * mouseout

* 變數說明:
    * **currX**,**currY**,**prevX**,**prevY**,**centerX**,**centerY** : 滑鼠位置，透過**addEeventListener**取得mouse的資料去減canvas的offeset

        ```javascript=
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
        ```
    * **dataURL**,**dataURL_forsize** : 儲存canvas.todataURL()
    * **canvas** : 取得canvas物件

        ```javascript=
            canvas = document.getElementById('can');
        ```
    * **ctx** : 取得canvas的2D內容

        ```javascript=
            ctx = canvas.getContext("2d");
        ```
    * **type** : 紀錄現在的畫筆型態，型態分別如下清單字面所示
        1. paint (default)
        2. circle
        3. rect
        4. triangle
        5. line
        6. text
        7. img
    * **paint_color**,**precolor** : 紀錄畫筆顏色
    * **paintsize** : 紀錄畫筆大小

---

## Function
* **Paint** : 畫筆是利用**lineTo()**串接起來實作的，步驟如下
    1. mousedown時，會呼叫一次draw()，確保使用者要畫一個點是可以被允許的
    2. 之後在mousemove時，會利用prevX,prevY和currX,currY去畫出一小段直線
    3. 重複第二步，直到mouseup或mouseout

    ```javascript=
       function draw() {
            ctx.beginPath();
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(currX, currY);
            ctx.strokeStyle = painter_color;
            ctx.lineWidth = paintsize;
            ctx.stroke();
            ctx.closePath();
        }
     ```

* **Circle** : 圖形使用**arc()**去繪畫，而實作步驟如下
    1. 在偵測mousedown時記錄中心點並用toDataURL()儲存背景
    2. 在mousemove時去計算半徑，繪畫部分則是先load回背景圖後再呼叫drawcircle()
    3. 當偵測到mouseup或mouseout時就直接呼叫drawcircle()

    ```javascript=
        function drawcircle() {
            var radius = Math.sqrt((Math.pow((currX-centerX), 2)) + Math.pow((currY-centerY), 2) );
            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI,false);
            ctx.fillStyle = painter_color;
            ctx.fill();
            ctx.strokeStyle = painter_color;
            ctx.lineWidth = paintsize;
            ctx.stroke();
            ctx.closePath();
        }
    ``` 

* **Rectangle** : 使用**rect()**去繪畫，對滑鼠動作的處理與Circle雷同
```javascript=
    function drawrect(){
        ctx.beginPath();
        ctx.moveTo(centerX,centerY);
        ctx.rect(centerX,centerY,currX-centerX,currY-centerY);
        ctx.fillStyle = painter_color;
        ctx.fill();
        ctx.strokeStyle = painter_color;
        ctx.lineWidth = paintsize;
        ctx.stroke();
        ctx.closePath();
    }
```

* **Triangle** : 使用**lineTo()**配合**closePath()**去繪畫，對滑鼠動作的處理與Circle雷同
```javascript=
    function drawtriangle(){
        var length = Math.sqrt((Math.pow((currX-centerX), 2)) + Math.pow((currY-centerY), 2) );
        ctx.beginPath();
        ctx.moveTo(centerX,centerY - length);
        ctx.lineTo(currX,currY);
        ctx.lineTo(currX - length*Math.sqrt(3),currY);
        ctx.closePath();
        ctx.strokeStyle = painter_color;
        ctx.lineWidth = paintsize;
        ctx.fillStyle = painter_color;
        ctx.fill();
        ctx.stroke();
    }
```

* **Line** : 單純使用**lineo()**去實作直線，對滑鼠動作的處理與Circle雷同
```javascript=
    function drawline(){
        ctx.beginPath();
        ctx.moveTo(centerX,centerY);
        ctx.lineTo(currX,currY);
        ctx.strokeStyle = painter_color;
        ctx.lineWidth = paintsize;
        ctx.stroke();
        ctx.closePath();
    }
```

* **畫筆粗細** : 透過調整**lineWidth**這個參數去改變粗細，當使用者改變type或color時。都會讀取html裡paintsize的value儲存至JS的paintsize變數

* **畫筆顏色** : 和畫筆粗細的做法雷同，將參數代入**strokeStyle**，再html裡利用input type="color"去實作出調色盤，並利用oninput()去呼叫更改顏色的function
```html=
    <input type="color" id="paintcolor" value="52602000000" oninput="color(this.value)">
```

* **Upload** : 利用html上傳檔案的方式，讓電腦裡的圖片可以暫存至html裡並預覽，程式碼如下
```html=
    <form action="/somewhere/to/upload" enctype="multipart/form-data">
        <input type="file" onchange="readURL(this)" targetID="preview_img" accept="image/gif, image/jpeg, image/png"
            />
        <img id="preview_img" src="#" style="width:100px;height:100px;" />
    </form>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var imageTagID = input.getAttribute("targetID");
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = document.getElementById(imageTagID);
                    img.setAttribute("src", e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
```

* **Image** : 取得**Upload**上傳的圖片，然後利用**drawImage()**將圖片印在canvas上
```javascript=
    function drawimage(){
        var base_image = document.getElementById('preview_img');
        ctx.drawImage(base_image, currX, currY ,150,150);
    }
```

* **Text** : 取得**text_input**裡的文字，透過**textformat**和**textsize**去更改字型和大小，最後利用**strokeText()**將文字印在canvas上
```javascript=
    function drawtext(){
        var textsize = document.getElementById('textsize').value;
        var textformat = document.getElementById('textformat').value;
        var text = document.getElementById('text_input').value;
        ctx.font = textsize + "px "+ textformat;
        ctx.lineWidth = 2;
        ctx.strokeText(text,currX,currY);
    }
```

* **Clean** : 會先跳出小視窗讓使用者確認是否確定要清空，確定之後將canvas清除
```javascript=
    function clean() {
        var m = confirm("Sure to clean the canvas?");
        if (m) {
            ctx.clearRect(0, 0, w, h);
            save_tmp();
            key_down = false;
        }
    }
```

* **Save** : 利用**canvas.todataURL()**去儲存目前canvas顯示的畫面
```javascript=  
    function save() {
        dataURL = canvas.toDataURL();
    }
```

* **Load** : 利用**save**存下的畫面，搭配**drawImage()**將畫面回復
```javascript=
    function load(){
        var imageObj = new Image();
            imageObj.onload = function() {
            ctx.clearRect(0, 0, w, h);
            ctx.drawImage(this, 0, 0);
        };
        imageObj.src = dataURL;
    }
```

* **Undo** : 利用與**save()**、**load()**類似的方式去實作，步驟如下
    1. 每完成一步動作後，都會將目前canvas的圖片push到**dataset**這個array裡
    2. 然後按下undo button後，會呼叫**undo()**
    3. undo 會先將畫面清空，然後在依**step**這個當作**dataset**的index去看要load哪個圖片
        ```javascript=
            function undo(){
            
                if(step>1) step--;

                var imageObj = new Image();
                    imageObj.onload = function() {
                    ctx.clearRect(0, 0, w, h);
                    ctx.drawImage(this, 0, 0);
                };

                if(step == 1){
                    ctx.clearRect(0, 0, w, h);
                } 
                else imageObj.src = dataset[step];
            }
        ```

* **Redo** : 利用**dataset**所記錄的資料去回復canvas
```javascript=
    function redo(){
        if(step < dataset.length-1){
            step++;
            var imageObj = new Image();
                imageObj.onload = function() {
                ctx.clearRect(0, 0, w, h);
                ctx.drawImage(this, 0, 0);
            };

            imageObj.src = dataset[step];
        }
    }
```

* **Download** : 利用 \<a\> 預設的download方式去下載canvas.toDataURL()，實作方法如下
```html=
    <a href="#" download="canvas.jpg" onclick="this.href=canvas.toDataURL();" style="background-color:white ;font-size:25px">下載</a>
```

