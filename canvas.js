var canvas, ctx,  
    currX = 0,
    centerX = 0,
    prevY = 0,
    currY = 0,
    centerY = 0,
    key_down = false.
    dot_flag = false;

var painter_color = "black", paintsize = 15;
var type = "paint";
var dataURL = false,dataURL_forsize = false;
var dataset = [];
var step = 1;
var precolor = "black";

function init() {
    canvas = document.getElementById('can');
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;

    canvas.addEventListener("mousemove", function (e) {
        EventContoller('move', e)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        EventContoller('down', e)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        EventContoller('up', e)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        EventContoller('out', e)
    }, false);
}
function color(color) {
    if (color == "white"){
        type = "paint";
        paintsize = document.getElementById('paintsize').value;;
        precolor = painter_color;
        painter_color = color ;
    }
    else{
        painter_color = color;
        paintsize = document.getElementById('paintsize').value;
    } 
}

function clean() {
    var m = confirm("Sure to clean the canvas?");
    if (m) {
        ctx.clearRect(0, 0, w, h);
        save_tmp();
        key_down = false;
    }
}

function save() {
    dataURL = canvas.toDataURL();
}
function save_tmp(){
    dataURL_forsize = canvas.toDataURL();
}
function load(){
    var imageObj = new Image();
        imageObj.onload = function() {
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(this, 0, 0);
    };
    imageObj.src = dataURL;
}

function load_tmp(){
    var imageObj = new Image();
        imageObj.onload = function() {
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(this, 0, 0);
    };
    imageObj.src = dataURL_forsize;
}

function undo(){
    
    if(step>1) step--;

    var imageObj = new Image();
        imageObj.onload = function() {
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(this, 0, 0);
    };

    if(step == 1){
        ctx.clearRect(0, 0, w, h);
    } 
    else imageObj.src = dataset[step];
}

function redo(){
    if(step < dataset.length-1){
        step++;
        var imageObj = new Image();
            imageObj.onload = function() {
            ctx.clearRect(0, 0, w, h);
            ctx.drawImage(this, 0, 0);
        };

        imageObj.src = dataset[step];
    }
}

function changetype(input){
    if(painter_color == "white"){
        color(precolor);
    }else{
        color(painter_color);
    }
    type = input;
}

function draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = paintsize;
    ctx.stroke();
    ctx.closePath();
}

function drawcircle() {
    var radius = Math.sqrt((Math.pow((currX-centerX), 2)) + Math.pow((currY-centerY), 2) );
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI,false);
    ctx.fillStyle = painter_color;
    ctx.fill();
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = paintsize;
    ctx.stroke();
    ctx.closePath();
}

function drawrect(){
    ctx.beginPath();
    ctx.moveTo(centerX,centerY);
    ctx.rect(centerX,centerY,currX-centerX,currY-centerY);
    ctx.fillStyle = painter_color;
    ctx.fill();
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = paintsize;
    ctx.stroke();
    ctx.closePath();
}

function drawtext(){
    var textsize = document.getElementById('textsize').value;
    var textformat = document.getElementById('textformat').value;
    var text = document.getElementById('text_input').value;
    ctx.font = textsize + "px "+ textformat;
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = 2;
    ctx.strokeText(text,currX,currY);
}

function drawline(){
    ctx.beginPath();
    ctx.moveTo(centerX,centerY);
    ctx.lineTo(currX,currY);
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = paintsize;
    ctx.stroke();
    ctx.closePath();
}

function drawtriangle(){
    var length = Math.sqrt((Math.pow((currX-centerX), 2)) + Math.pow((currY-centerY), 2) );
    ctx.beginPath();
    ctx.moveTo(centerX,centerY - length);
    ctx.lineTo(currX,currY);
    ctx.lineTo(currX - length*Math.sqrt(3),currY);
    ctx.closePath();
    ctx.strokeStyle = painter_color;
    ctx.lineWidth = paintsize;
    ctx.fillStyle = painter_color;
    ctx.fill();
    ctx.stroke();
}

function drawimage(){
    var base_image = document.getElementById('preview_img');
    ctx.drawImage(base_image, currX, currY ,150,150);
}

function EventContoller(res, e) {
    if (res == 'down') {
        prevX = currX;
        prevY = currY;
        currX = e.clientX - canvas.offsetLeft;
        currY = e.clientY - canvas.offsetTop;

        key_down = true;
        dot_flag = true;
        if (dot_flag) {
            step++;
            switch(type){
                case "paint":
                    ctx.beginPath();
                    ctx.fillStyle = painter_color;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                    break; 
                case "text":
                    drawtext();
                    break;
                default:
                    centerX = currX;
                    centerY = currY;
                    save_tmp();
                    break;
            }
        }
    }
    if (res == 'up' || res == "out") {
        
        for(var i =step;i<dataset.length;){
            dataset.pop();
        }

        var tmpdataURL = canvas.toDataURL();
        dataset.push(tmpdataURL);
        if(key_down){
            key_down = false;
            switch(type){
                case "circle":
                    drawcircle();
                    break;
                case "triangle":
                    drawtriangle();
                    break;
                case "rect":
                    drawrect();
                    break;
                case "line":
                    drawline();
                    break;
                case "img":
                    drawimage();
                    break;
                default:
                    break;
            }
        }
    }
    if (res == 'move') {
        if (key_down) {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            
            switch(type){
                case "paint":
                    draw();
                    break;
                case "circle":
                    load_tmp();
                    drawcircle();
                    break;
                case "triangle":
                    load_tmp();
                    drawtriangle();
                    break;
                case "rect":
                    load_tmp();
                    drawrect();
                    break;
                case "line":
                    load_tmp();
                    drawline();
                    break;
                case "img":
                    load_tmp();
                    drawimage();
                    break;
            }
        }
    }
}